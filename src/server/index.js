import Koa from 'koa';
import React from 'React';
import ReactDOMServer from 'react-dom/server';
import Html from './Html';
const app = new Koa();

export default ({title, url}) => {
  app.use(async (ctx, next) => {
    const start = new Date();
    await next();
    const ms = new Date() - start;
    ctx.set('X-Response-Time', `${ms}ms`);
  });

  app.use(async (ctx, next) => {
    await next();
    const html = ReactDOMServer.renderToStaticMarkup(<Html title={title} url={url} body={ctx.pageContents} />);
    ctx.body = html;
  });

  app.use(async (ctx, next) => {
    const html = 'hello koa';
    ctx.pageContents = html;
  });

  return app;
};
