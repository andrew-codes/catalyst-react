import React from 'react';
import { render } from 'react-dom';
import { App } from './../components/HelloWorld';

render(<App />, document.getElementById('root'));
