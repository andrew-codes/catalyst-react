import React, {Component, PropTypes} from 'react';

export default class HelloWorld extends Component {
	static propTypes = {};

	static defaultProps = {};

	constructor(props, context) {
		super(props, context);
	}

	render() {
		return (
			<div>Hello koa</div>
		);
	}
}
