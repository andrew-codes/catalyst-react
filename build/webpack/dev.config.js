import path from 'path';
import webpack from 'webpack';
import {rootDir, srcDir} from './../../env';

export default {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'eventsource-polyfill', // necessary for hot reloading with IE
    'webpack-hot-middleware/client',
    path.join(srcDir, 'client', 'index')
  ],
  output: {
    path: __dirname + '/dist',
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      include: srcDir,
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'react', 'stage-3', 'stage-2', 'stage-1', 'react-hmre'],
        plugins: ['transform-class-properties']
      }
    }]
  }
};
