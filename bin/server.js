require('babel-register');

const config = require('./../build/config');
const server = require('./../src/server/main');
const debug = require('debug')('app:bin:server');

const port = config.port;
const host = config.host;

server.listen(port);
debug(`Server is now running at ${host}:${port}.`);
